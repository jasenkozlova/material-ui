import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import DeleteIcon from '@material-ui/icons/Delete';

const columns = [
  { id: 'title', label: 'Name', minWidth: 270 },
  { id: 'count', label: 'Count', minWidth: 50, align: 'center'},
  { id: 'price', label: 'Price', minWidth: 50, align: 'right'},
  {
    id: 'cost',
    label: 'Cost',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toFixed(2),
  },
  { id: 'del', label: 'Delete', maxWidth: 20, align: 'right'},
];

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
  flex: {
    display: 'flex'
  }
});

export default function PurchesedTable(props) {
  const classes = useStyles();
  return (
    <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {props.cart.map((row) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                  {columns.map((column) => {
                    const value = row[column.id];
                    if (column.id === 'count') {
                        return (<TableCell key={column.id} align={column.align}>
                                    <Button onClick={() => props.removed(row.id, 'cart')} disabled={value <= 1? true : false}>
                                        <RemoveCircleIcon fontSize="large" />
                                    </Button>
                                    <span>{value}</span>
                                    <Button>
                                        <AddCircleIcon onClick={() => props.added(row.id, 'cart')} fontSize="large"/>
                                    </Button>
                        </TableCell>);
                    } else if (column.id === 'del') {
                        return (
                          <TableCell align={column.align} key={column.id} >
                            <Button onClick={() => props.deleted(row.id, props.userId)}>
                                <DeleteIcon fontSize="large" />
                            </Button>
                          </TableCell>
                        )
                    } else { 
                      return (
                        <TableCell key={column.id} align={column.align}>
                          {column.format && typeof value === 'number' ? column.format(value) : value}
                      </TableCell>)}
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
}

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    minWidth: 240,
    margin: '10px'
  },
  cartActions: {
    justifyContent: 'center'
  }, 
  desc: {
    height: '80px'
  },
  message: {
    width: '100%',
    textAlign: 'center',
    marginBottom: '30px'
  }
});

export default function ProductCard(props) {
  const classes = useStyles();
  const description = props.desc.length > 150 ? props.desc.slice(0, 150) + '...': props.desc;
  const cardActions = ! props.cartIDs.some(item => item===props.id) ? 
    (<CardActions className={classes.cartActions}>
      <Button 
      disabled={props.counter <= 0? true : false}
      onClick={() => props.removed(props.id, 'products')}>
        <RemoveCircleIcon fontSize="large" />
      </Button>
      <div>{props.counter}</div>
      <Button  onClick={() => props.added(props.id, 'products')}>
        <AddCircleIcon fontSize="large"/>
      </Button>
      <Typography variant="h6" color="secondary" component="p">
        {props.cost? props.cost.toFixed(2) : ''}
      </Typography>
      <Button onClick={() => props.ordered(props.id, props.counter, props.userId, props.price, props.title)} disabled={props.counter <= 0 || props.inCart ? true : false}>
        <Typography variant="button">
          BUY
        </Typography>
      </Button>
    </CardActions>)
    :  <Typography className={classes.message} variant="h6" color="secondary" component="p">ALREADY IN CART</Typography>

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          component="img"
          alt={props.title}
          height="240"
          image={props.image}
          title={props.title}
        />
        <CardContent>
          <React.Fragment>
            <Typography gutterBottom variant="h5" component="h2">
              {props.title}
            </Typography>
            <Typography gutterBottom variant="h6" color="secondary" component="h2">
              {props.price.toFixed(2)}
            </Typography>
          </React.Fragment>
          <Typography className={classes.desc} variant="body2" color="textSecondary" component="p">
            {description}
          </Typography>
        </CardContent>
      </CardActionArea>
      {cardActions}
    </Card>
  );
}
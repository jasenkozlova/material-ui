import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles((theme) => ({
  nav: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
        display: 'block',
        position: 'fixed',
        marginTop: '70px',
    },
  },
  link: {
      marginLeft: theme.spacing(2),
      color: '#005566',
      textDecoration: 'none',
      textTransform: 'uppercase',
      width: '150px',
      height: '64px',
      lineHeight: '64px'
    },
    space: {
        [theme.breakpoints.up('md')]: {
          width: '200px'
        }
    }
}));

export default function ProdNav(props) {
  const classes = useStyles();

  return (
      <div>
        <Box className={classes.nav}>
            {props.categories.map(item =>
            <div key={'link-'+item.cat}>
                <Link href={'#'+item.cat}  className={classes.link}>{item.title}</Link>
                </div>)}
        </Box> 
        <div className={classes.space}></div>
  </div>
  );
}

import React from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Badge from '@material-ui/core/Badge';
import * as actions from '../../store/actions/index';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import SwipebleTampDrawer from './Swipeble/SwipebleTempDrawer';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex'
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  link: {
      marginLeft: theme.spacing(2),
      color: '#ffffff',
      textDecoration: 'none',
      textTransform: 'uppercase',
      width: '150px',
      height: '64px',
      lineHeight: '64px',
      textAlign: 'center',
      '&:hover': {
        backgroundColor: fade(theme.palette.common.white, 0.25),
        height: '62px',
        borderBottom: 'solid 2px #fff'
      },
    },
    authLink: {
      marginLeft: theme.spacing(2),
      color: '#ffffff',
      textDecoration: 'none',
      '&:hover': {
        backgroundColor: fade(theme.palette.common.white, 0.25),
      },
    },
    activeLink: {
      backgroundColor: fade(theme.palette.common.white, 0.25),
      height: '62px',
      borderBottom: 'solid 2px #fff'
    }
}));

export function PrimarySearchAppBar(props) {
  const classes = useStyles();

  const signInLink = props.isAuthenticated? 
    <NavLink
      className={classes.authLink}
      to="/auth">Sign in</NavLink>
    : <IconButton>
        <ExitToAppIcon htmlColor='#ffffff' onClick={props.logout}/>
      </IconButton>;
  return (
    <div className={classes.grow}>
      <AppBar position="fixed">
        <Toolbar>
          <div className={classes.sectionMobile}>
            <SwipebleTampDrawer/>
          </div>
          <Typography className={classes.title} variant="h6" noWrap>
            Material-UI
          </Typography>
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            <NavLink
              className={classes.link}
              to="/"
              exact activeClassName={classes.activeLink}>All products</NavLink> 
            <NavLink
              className={classes.link}
              to="/favorites" activeClassName={classes.activeLink}>Favorites</NavLink> 
          </div>
          <div className={classes.grow} />
          <div>
            <NavLink to="/cart">
              <IconButton>
                <Badge badgeContent={props.counter} color="secondary">
                  <ShoppingCartIcon htmlColor='#ffffff'/>
                </Badge>
              </IconButton>
              </NavLink> 
              {signInLink}
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}

const mapStateToProps = state => {
  return {
    counter : state.cart.cart.length,
    isAuthenticated: !state.auth.isAnonim}
}

const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch(actions.logout()),
  };
};

 
export default connect(mapStateToProps, mapDispatchToProps)(PrimarySearchAppBar)
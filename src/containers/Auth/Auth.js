import React, { useState, useEffect} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import * as actions from '../../store/actions/index';
import { connect } from 'react-redux';
import { updateObject, checkValidity } from '../../store/utility';
import { Redirect } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        React practice by Yana Kozlova
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  ); 
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(12),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  alignLeft: {
    textAlign: 'left',
  },
  error: {
    color: 'red',
    width: '100%',
    textAlign: 'center',
  }
}));

const Auth = (props) => {
  const classes = useStyles();

  const [authForm, setAuthForm] = useState({
    email: {
      elementType: 'input',
      elementConfig: {
        type: 'email',
        placeholder: 'Mail Address'
      },
      value: '',
      validation: {
        required: true,
        isEmail: true
      },
      valid: true,
      touched: false
    },
    password: {
      validation: {
        required: true,
        minLength: 6
      },
      valid: true,
      touched: false,
      showPassword: false,
    }
  });
  const [isSignup, setIsSignup] = useState(props.isAuthenticated);
  const submitHandler = event => {
    event.preventDefault();
    props.onAuth(authForm.email.value, authForm.password.value, isSignup);
  };

  const resetHandler = event => {
    event.preventDefault();
    props.onSendResetPassEmail(authForm.email.value);
  };

  useEffect(() => {
    if (props.authRedirectPath !== '/') {
      props.onSetAuthRedirectPath();
    }
  }, []);

  const switchAuthModeHandler = () => {
    setIsSignup(!isSignup);
  };

  const handleClickShowPassword = () => {
    const updatedControls = updateObject(authForm, {
      password: updateObject(authForm, {
        showPassword: !authForm.password.showPassword
      })
    });
    setAuthForm(updatedControls);
  };

  const inputChangedHandler = (event, controlName) => {
    const updatedControls = updateObject(authForm, {
      [controlName]: updateObject(authForm[controlName], {
        value: event.target.value,
        valid: checkValidity(
          event.target.value,
          authForm[controlName].validation
        ),
        touched: true
      })
    });
    setAuthForm(updatedControls);
  };
  let loader;

  if (props.loading) {
    loader = <CircularProgress />;
  }

  let errorMessage = null;

  if (props.error) {
    errorMessage = <p className={classes.error}>{props.error.message}</p>;
  }
 
  let authRedirect = null;
  if (props.isAuthenticated && props.isVerified) {
    authRedirect = <Redirect to={props.authRedirectPath} />;
  }
  let form = null;

  if (props.isAuthenticated && !props.isVerified && !props.isAnonim) {
    form = <p className={classes.error}>Success! Please, verify your email</p>;
  } else {
    form= <><TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
              error={!authForm.email.valid && authForm.email.touched}
              onChange={( event ) => inputChangedHandler( event, 'email' )} 
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type={authForm.password.showPassword ? 'text' : 'password'}
              id="password"
              autoComplete="current-password"
              value={authForm.password.value}
              error={!authForm.password.valid && authForm.password.touched}
              onChange={( event ) => inputChangedHandler( event, "password" )} 
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end" onClick={handleClickShowPassword}>
                    {authForm.password.showPassword ? <Visibility /> : <VisibilityOff />}
                  </InputAdornment>
                ),
              }}
            />
            {errorMessage}
            {authRedirect}
            {loader}
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              {isSignup? "Sign Up" : "Sign In"}
            </Button>
            <Grid container className={classes.alignLeft}>
          {isSignup? '' : <Grid item xs>
              <Link href="#" variant="body2" onClick={resetHandler}>
                Forgot password?
              </Link>
          </Grid>}
            <Grid item>
              <Link href="#" variant="body2" onClick={switchAuthModeHandler}>
                {isSignup? "Already have an account? Sign in" : "Don't have an account? Sign up"}
              </Link>
            </Grid>
          </Grid></>
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          {isSignup? 'Sign up' : 'Sign in'}
        </Typography>
        <form className={classes.form} onSubmit={submitHandler}>
          {form}
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    isAuthenticated: state.auth.token !== null,
    isAnonim: state.auth.isAnonim,
    isVerified: state.auth.verified,
    authRedirectPath: state.auth.authRedirectPath
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onAuth: (email, password, isSignup) =>
      dispatch(actions.auth(email, password, isSignup)),
    onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath('/')),
    onSendResetPassEmail: (email) => dispatch(actions.sendResetPassEmail(email))
  };
};

export default connect( mapStateToProps, mapDispatchToProps )( Auth );


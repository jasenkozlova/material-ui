import Container from '@material-ui/core/Container';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import axios from '../../axios';
import * as actions from '../../store/actions/index';
import PurchesedTable from '../../components/PurchesedTable/PurchesedTable';
import Typography from '@material-ui/core/Typography';

const Cart = (props) => {

    /*useEffect(() => {
        if (props.cart) props.cart.map((item, index) => props.onFetching(item.id, item.cartId, item.count, index));
      }, [props.cart]);*/
    let emptyCart, purchProducts, cartCost;
    if (!props.cartNumber)  {
        emptyCart = <Typography variant="h4" color="primary"> Cart is empty!</Typography> ;
    } else { 
        purchProducts = <PurchesedTable 
                            cart={props.cart} 
                            removed={props.onDecrement}
                            added={props.onIncrement}
                            deleted={props.onDelete}
                            userId={props.userId}/>;
        cartCost = <>{props.cart.reduce((fCost, item) => {return fCost + item.cost}, 0).toFixed(2)}</>

    }
    return (
        <Container style={{ marginTop: '70px'}} maxWidth='lg'>
            {purchProducts}
            {cartCost}
            {emptyCart}
        </Container>) ;
};

const mapStateToProps = state => {
    return {
        cart: state.cart.cart,
        cartNumber: state.cart.number,
        userId: state.auth.userId
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onIncrement: (id, purpose) => dispatch(actions.incProduct(id, purpose)),
        onDecrement: (id, purpose) => dispatch(actions.decProduct(id, purpose)),
        onDelete: (id, userId) => dispatch(actions.deleteFromCart(id, userId)),
        onFetching: (prodId, cartId, count, index) => dispatch(actions.fetchCartProdInfo(prodId, cartId, count, index))
    };
};

export default connect( mapStateToProps, mapDispatchToProps )( Cart, axios );
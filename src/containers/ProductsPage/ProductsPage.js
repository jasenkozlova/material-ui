
import ProductCard from '../../components/ProductCard/ProductCard';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Skeleton from '@material-ui/lab/Skeleton';
import Divider from '@material-ui/core/Divider';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from '../../axios';
import * as actions from '../../store/actions/index';
import ProdNav from './../../components/Nav/ProductNavigation';
  
let categories = [
  { title: 'Lanches', cat: 'first' }, 
  { title: 'Deserts', cat: 'desert' }, 
  { title: 'Drinks', cat: 'drinks' }, 
];;

class ProductsPage extends Component {
  state = {
    purchasing: false
  }
  componentDidMount () {
    this.props.onFetchProducts();
    //if (!this.props.productList.length) this.props.onFetchProducts();
  }

  purchaseHandler = () => {
      this.setState( { purchasing: true } );
      this.props.onInitPurchase();
       //this.props.history.push('/checkout');
  }

  render () {
    const cartIDs = this.props.cart.map(item => item.prodId);
    const products = this.props.loading ? 
      categories.map(item =>
        <Box key={'products-'+item.cat}>
          <Typography variant='h4' align='left' id={item.cat} style={{ paddingTop: '70px'}}>{item.title}</Typography>
          <Divider/>
          <Box style={{display: 'flex', flexWrap: 'wrap'}}>
            <Skeleton variant="rect" animation="wave" width={345} height={240} style={{margin: '10px'}} />
            <Skeleton variant="rect" animation="wave" width={345} height={240} style={{margin: '10px'}}/>
            <Skeleton variant="rect" animation="wave" width={345} height={240} style={{margin: '10px'}}/>
          </Box>
        </Box>)
      : 
      categories.map(item =>
        <Box key={'products-'+item.cat}>
          <Typography variant='h4' align='left' id={item.cat} style={{ paddingTop: '70px'}}>{item.title}</Typography>
          <Divider/>
          <Box style={{display: 'flex', flexWrap: 'wrap'}}>
          {this.props.productList.filter(prod => prod.category === item.cat).map(prod =>
            <ProductCard
              key={prod.id}
              id={prod.id}
              title={prod.title}
              desc={prod.desc}
              image={prod.photo}
              price={prod.price}
              weight={prod.weight}
              counter={prod.count}
              inCart={prod.inCart}
              cost={prod.cost}
              added={this.props.onIncrement}
              removed={this.props.onDecrement}
              ordered={this.props.onAddToCart}
              cartIDs={cartIDs}
              userId={this.props.userId}/>
          )}
          </Box>
        </Box>);
    console.log(this.props.loading);
      return ( 
        <Container style={{ display: 'flex'}} maxWidth='xl'>
          <ProdNav categories={categories}/>
          <Box>
            {products}
          </Box>
        </Container>)
    }
  }

const mapStateToProps = state => {
    return {
        productList: state.allProducts.products,
        cart: state.cart.cart,
        loading: state.allProducts.loading,
        userId: state.auth.userId
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchProducts: () => dispatch( actions.fetchProducts()),
        onIncrement: (id, purpose) => dispatch(actions.incProduct(id, purpose)),
        onDecrement: (id, purpose) => dispatch(actions.decProduct(id, purpose)),
        onAddToCart: (id, count, userId, price, title) => dispatch(actions.addToCart(id, count, userId, price, title)),
    };
};

export default connect( mapStateToProps, mapDispatchToProps )( ProductsPage, axios );
import React from 'react';
import ReactDOM from 'react-dom';
import * as firebase from "firebase/app";
import "firebase/auth";
import './index.css';
import App from './App';
import { Provider } from 'react-redux';
import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from 'react-router-dom';
import reducer from './store/reducers/products';
//import {counterReducer as counter} from './store/reducers/counter';
import authReducer from './store/reducers/auth';
import cartReducer from './store/reducers/cart';
//import orderReducer from './store/reducers/orders';
import { reducer as SignForm } from 'redux-form';
import thunk from 'redux-thunk';

const rootReducer = combineReducers({
  allProducts: reducer,
  auth: authReducer,
  cart: cartReducer
  //order: orderReducer,
});

const store = createStore(rootReducer, composeWithDevTools(
  applyMiddleware(thunk),
  // other store enhancers if any
));
//const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; 

//const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

console.log(store.getState());
ReactDOM.render(
  <Provider store={store}>
    <React.StrictMode>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </React.StrictMode>
  </Provider>,
  document.getElementById('root')
);



// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

const firebaseConfig = {
  apiKey: "AIzaSyBWY3S1OUDnx-nl_iV9YZDWXz0LLC2v09k",
  authDomain: "prod-yana.firebaseapp.com",
  databaseURL: "https://prod-yana.firebaseio.com",
  projectId: "prod-yana",
  storageBucket: "prod-yana.appspot.com",
  messagingSenderId: "986115328631",
  appId: "1:986115328631:web:20a74648d5b02dfcef5472",
  measurementId: "G-5738JQJ9KM"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
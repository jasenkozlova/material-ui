import React, { useEffect, Suspense } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from './store/actions/index';
import ProductsPage from './containers/ProductsPage/ProductsPage';
import Navigation from './components/Nav/Navigation';

const Cart = React.lazy(() => {
  return import('./containers/Cart/Cart');
});

const Auth = React.lazy(() => {
  return import('./containers/Auth/Auth');
});

const App = (props) => {
  useEffect(() => {
    props.onTryAutoSignup();
  }, []);

  useEffect(() => {
    if (props.userId) props.onGettingCart(props.userId);
  }, [props.userId]);

  let routes = (
    <Switch>
        <Route path="/" component={ProductsPage} exact />
        <Route path="/auth" render={props => <Auth {...props} />} />
        <Route path="/cart" render={props => <Cart {...props} />} />
      <Redirect to="/" />
    </Switch>
  );

  if (props.isAuthenticated && props.isVerifyicated) {
    routes = (
      <Switch>
        <Route path="/" component={ProductsPage} exact />
        <Route path="/cart" render={props => <Cart {...props} />} />
        <Redirect to="/" />
      </Switch>
    );
  }

  return (
    <div>
        <Navigation/>
        <Suspense fallback={<p>Loading...</p>}>{routes}</Suspense>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null,
    isVerifyicated: state.auth.verified,
    userId: state.auth.userId
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch(actions.authCheckState()),
    onGettingCart: (userId) => dispatch(actions.fetchCart(userId))
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(App)
);

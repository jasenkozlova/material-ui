import * as actionTypes from './actionTypes';
import axios from '../../axios';


export const incProduct = ( id, purpose ) => {
    return {
        type: actionTypes.INCREMENT,
        id: id,
        purpose: purpose
    };
};

export const decProduct = ( id, purpose ) => {
    return {
        type: actionTypes.DECREMENT,
        id: id,
        purpose: purpose
    };
};

/*export const delProduct = ( id, purpose ) => {
    return {
        type: actionTypes.DELETE_PRODUCT,
        id: id,
        purpose: purpose
    };
};*/


export const fetchProductsSuccess = ( products ) => {
    return {
        type: actionTypes.FETCH_PRODUCTS_SUCCESS,
        products: products
    };
};

export const fetchProductsFail = ( error ) => {
    return {
        type: actionTypes.FETCH_PRODUCTS_FAIL,
        error: error
    };
};

export const fetchProductsStart = () => {
    return {
        type: actionTypes.FETCH_PRODUCTS_START
    };
};

export const fetchProducts = () => {
    return dispatch => {
        dispatch(fetchProductsStart());
        axios.get( '/products.json' )
            .then( res => {
                const fetchedProducts = [];
                for ( let key in res.data ) {
                    fetchedProducts.push( {
                        ...res.data[key],
                        id: key,
                        count: 1
                    } );
                }
                dispatch(fetchProductsSuccess(fetchedProducts));
            } )
            .catch( err => {
                dispatch(fetchProductsFail(err));
            } );
    };
};

export const purchaseProduct = ( id ) => {
    return {
        type: actionTypes.PURCHASE_PRODUCT,
        id: id      
    };
};
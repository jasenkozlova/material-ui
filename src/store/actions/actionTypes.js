export const AUTH_START = 'AUTH_START';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_FAIL = 'AUTH_FAIL';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';
export const RESET_PASSWORD = 'RESET_PASSWORD';
export const GET_USER_INFO = 'GET_USER_INFO';
export const SET_AUTH_REDIRECT_PATH = 'SET_AUTH_REDIRECT_PATH';

export const ADD_TO_CART = 'ADD_TO_CART';
export const ADD_TO_CART_START = 'ADD_TO_CART_START';
export const ADD_TO_CART_SUCCESS = 'ADD_TO_CART_SUCCESS';
export const ADD_TO_CART_FAIL = 'ADD_TO_CART_FAIL';
export const FETCH_CART_NUMBER = 'FETCH_CART_NUMBER';
export const FETCH_CART_FAIL = 'FETCH_CART_FAIL';
export const FETCH_CART_PROD_INFO_SUCCESS = 'FETCH_CART_PROD_INFO_SUCCESS';
export const FETCH_CART_PROD_INFO_FAIL = 'FETCH_CART_PROD_INFO_FAIL';
export const CART_DELETE_PRODUCT = 'CART_DELETE_PRODUCT';

export const PURCHASE_PRODUCT = 'PURCHASE_PRODUCT';
//export const PURCHASE_SUCCESS = 'PURCHASE_SUCCESS';
//export const PURCHASE_FAIL = 'PURCHASE_FAIL';
//export const PURCHASE_INIT = 'PURCHASE_INIT';
export const FETCH_PRODUCTS_START = 'FETCH_PRODUCTS_START';
export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const FETCH_PRODUCTS_FAIL = 'FETCH_PRODUCTS_FAIL';
export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';

//export const FETCH_ORDERS_START = 'FETCH_ORDERS_START';
//export const FETCH_ORDERS_SUCCESS = 'FETCH_ORDERS_SUCCESS';
//export const FETCH_ORDERS_FAIL = 'FETCH_ORDERS_FAIL';
import * as actionTypes from './actionTypes';
import axios from '../../axios';
import {purchaseProduct as purchaseProduct} from './index';


/*export const incCartProd = ( id ) => {
    return {
        type: actionTypes.INCREMENT,
        id: id
    };
};

export const decCartProd = ( id ) => {
    return {
        type: actionTypes.DECREMENT,
        id: id
    };
};*/

export const deleteCartProd = ( id ) => {
    return {
        type: actionTypes.CART_DELETE_PRODUCT,
        id: id
    };
};

export const fetchCartNumber = ( number ) => {
    return {
        type: actionTypes.FETCH_CART_NUMBER,
        number: number
    };
};

export const fetchCartFail = ( error ) => {
    return {
        type: actionTypes.FETCH_CART_FAIL,
        error: error
    };
};

export const fetchCart = (userId) => {
    return dispatch => {
        axios.get( '/cart/'+ userId +'.json' )
            .then( res => {
                for ( let key in res.data ) {
                    dispatch(fetchCartProdInfo(key, res.data[key].id, res.data[key].count));
                }
                dispatch(fetchCartNumber(Object.keys(res.data).length));
            } )
            .catch( err => {
                dispatch(fetchCartFail(err));
            } );
    };
};

export const fetchCartProdInfoSuccess = ( cartItem ) => {
    return {
        type: actionTypes.FETCH_CART_PROD_INFO_SUCCESS,
        cartItem: cartItem
    };
};

export const fetchCartProdInfoFail = ( error ) => {
    return {
        type: actionTypes.FETCH_CART_PROD_INFO_FAIL,
        error: error
    };
};

export const fetchCartProdInfo = (cartId, prodId, count) => {
    return dispatch => {
        axios.get( '/products/'+ prodId +'.json' )
            .then( res => {
                dispatch(fetchCartProdInfoSuccess({
                    id: cartId, 
                    count: count,
                    prodId: prodId,
                    cost: count*res.data.price,
                    price: res.data.price,
                    title: res.data.title
                }));
            } )
            .catch( err => {
                dispatch(fetchCartProdInfoFail(err));
            } );
    };
};

export const addToCartSuccess = ( id, prodId, count, price, title ) => {
    return {
        type: actionTypes.ADD_TO_CART_SUCCESS,
        id: id,
        prodId: prodId,
        count: count, 
        price: price,
        title: title
    };
};

export const addToCartFail = ( error ) => {
    return {
        type: actionTypes.ADD_TO_CART_FAIL,
        error: error
    };
}

export const addToCartStart = () => {
    return {
        type: actionTypes.ADD_TO_CART_START
    };
};

export const addToCart = ( id, count, userId, price, title ) => {
    return dispatch => {
        dispatch( addToCartStart() );
        axios.post( 'cart/' + userId + '.json', {id: id, count: count} )
            .then( response => {
                dispatch( addToCartSuccess( response.data.name, id, count, price, title ) );
                dispatch(purchaseProduct(id));
            } )
            .catch( error => {
                dispatch( addToCartFail( error ) );
            } );
    }; 
};

export const deleteFromCart = ( id, userId ) => {
    return dispatch => {
        console.log(id, userId);
        axios.delete( 'cart/' + userId + '/' + id + '.json')
            .then( response => {
                dispatch(deleteCartProd(id));
            } )
            .catch( error => {
                dispatch( addToCartFail( error ) );
            } );
    }; 
};

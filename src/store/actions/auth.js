import axios from 'axios';

import * as actionTypes from './actionTypes';

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    };
};

export const authSuccess = (token, userId, isAnonim) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        idToken: token,
        userId: userId,
        isAnonim: isAnonim
    };
};

export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    };
};

export const logout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('expirationDate');
    localStorage.removeItem('userId');
    localStorage.removeItem('anonimId');
    return {
        type: actionTypes.AUTH_LOGOUT
    };
};

export const deleteUser = (idToken) => {
    return dispatch => {axios.post('https://identitytoolkit.googleapis.com/v1/accounts:delete?key=AIzaSyBWY3S1OUDnx-nl_iV9YZDWXz0LLC2v09k', {idToken: idToken})
        .catch(err => {
            dispatch(authFail(err.response.data.error));
        });
    }
};
 
export const checkAuthTimeout = (expirationTime) => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logout());
            dispatch(anonimAuth());
        }, expirationTime * 1000);
    };
};

export const auth = (email, password, isSignup) => {
    return dispatch => {
        dispatch(authStart());
        const authData = {
            email: email,
            password: password,
            returnSecureToken: true
        };
        let url = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyBWY3S1OUDnx-nl_iV9YZDWXz0LLC2v09k';
        if (!isSignup) {
            url = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyBWY3S1OUDnx-nl_iV9YZDWXz0LLC2v09k';
        }
        axios.post(url, authData)
            .then(response => {
                const expirationDate = new Date(new Date().getTime() + response.data.expiresIn * 24000);
                localStorage.setItem('token', response.data.idToken);
                localStorage.setItem('expirationDate', expirationDate);
                localStorage.setItem('userId', response.data.localId);
                dispatch(authSuccess(response.data.idToken, response.data.localId, false));
                dispatch(sendVerifyEmail(response.data.idToken));
                dispatch(checkAuthTimeout(response.data.expiresIn));
                dispatch(getUserInfo(response.data.idToken));
            })
            .catch(err => {
                dispatch(authFail(err.response.data.error));
            });
    };
};

export const setAuthRedirectPath = (path) => {
    return {
        type: actionTypes.SET_AUTH_REDIRECT_PATH,
        path: path
    };
};

export const anonimAuth = () => {
    return dispatch => {
        axios.post(
            'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyBWY3S1OUDnx-nl_iV9YZDWXz0LLC2v09k', 
            {returnSecureToken : true})
            .then(response => {
                const expirationDate = new Date(new Date().getTime() + response.data.expiresIn * 24000);
                localStorage.setItem('token', response.data.idToken);
                localStorage.setItem('expirationDate', expirationDate);
                localStorage.setItem('anonim', true);
                localStorage.setItem('userId', response.data.localId);
                dispatch(authSuccess(response.data.idToken, response.data.localId, true));
                dispatch(checkAuthTimeout(response.data.expiresIn));
                //dispatch(fetchCart());
            })
            .catch(err => {
                dispatch(authFail(err.response.data.error));
            });
    };
};

export const sendVerifyEmail = (idToken) => {
    return dispatch => {
    axios.post('https://identitytoolkit.googleapis.com/v1/accounts:sendOobCode?key=AIzaSyBWY3S1OUDnx-nl_iV9YZDWXz0LLC2v09k', 
        {requestType: "VERIFY_EMAIL", idToken: idToken})
        .then()
        .catch(err => {
            dispatch(authFail(err.response.data.error));
        });
    }
};

export const sendResetPassEmail = (userEmail) => {
    return dispatch => {
    axios.post('https://identitytoolkit.googleapis.com/v1/accounts:sendOobCode?key=AIzaSyBWY3S1OUDnx-nl_iV9YZDWXz0LLC2v09k', 
        {requestType: "PASSWORD_RESET", email: userEmail})
        .then()
        .catch(err => {
            dispatch(authFail(err.response.data.error));
        });
    }
};

export const userInfo = (verified) => {
    return {
        type: actionTypes.GET_USER_INFO,
        verified: verified
    };
};

export const getUserInfo = (idToken) => {
    return dispatch => {
    axios.post('https://identitytoolkit.googleapis.com/v1/accounts:lookup?key=AIzaSyBWY3S1OUDnx-nl_iV9YZDWXz0LLC2v09k', 
        {idToken: idToken})
        .then(response => {
            dispatch(userInfo(response.data.users[0].emailVerified));
        })
        .catch(err => {
            console.log('err', err);
        });
    }
};


export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(anonimAuth());
        } else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            const anonim = localStorage.getItem('anonim');
            if ((expirationDate <= new Date() && anonim)) {
                dispatch(deleteUser(token));
                dispatch(logout());
                dispatch(anonimAuth());
            }
            if (expirationDate <= new Date() && !anonim) {
                dispatch(logout());
                dispatch(anonimAuth());
            } else {
                const userId = localStorage.getItem('userId');
                dispatch(authSuccess(token, userId, anonim));
                dispatch(checkAuthTimeout((expirationDate.getTime() - new Date().getTime()) / 1000 ));
                if (!anonim) dispatch(getUserInfo(token));
            }   
        }
    };
};
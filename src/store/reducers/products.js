import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    products: [],
    cart: [],
    loading: false
};

const fetchProductsStart = ( state, action ) => {
    return updateObject( state, { loading: true } );
};

const fetchProductsSuccess = ( state, action ) => {
    return updateObject( state, {
        products: action.products,
        loading: false
    } );
};

const fetchProductsFail = ( state, action ) => {
    return updateObject( state, { loading: false } );
};

const incProduct = ( state, action ) => {
  let index;
  if (action.purpose == 'products') {
    index = state.products.indexOf(state.products.find(prod => prod.id===action.id));
  } else {
    index = action.id;
  };
  const updatedProd = updateObject( state[action.purpose][index], { ['count']: state[action.purpose][index].count + 1, 
  ['cost']: (state[action.purpose][index].count + 1) * state[action.purpose][index].price })
  const prods = [];
  for ( let key in state[action.purpose] ) {
    key==index ?
      prods.push({...updatedProd })
      : prods.push({...state[action.purpose][key]});
  }
  if (action.purpose === 'cart') localStorage.setItem('cart', JSON.stringify(prods));
  const updatedState = {
    [action.purpose] : prods
  }
  return updateObject( state, updatedState );
};

const decProduct = (state, action) => {
  let index;
  if (action.purpose === 'products') {
    index = state.products.indexOf(state.products.find(prod => prod.id===action.id));
  } else {
    index = action.id;
  };
  const updatedProd = updateObject( state[action.purpose][index], { ['count']: state[action.purpose][index].count - 1,
  ['cost']: (state[action.purpose][index].count - 1) * state[action.purpose][index].price })
  const prods = [];
  for ( let key in state[action.purpose] ) {
    key==index?
      prods.push({...updatedProd })
      : prods.push({...state[action.purpose][key]});
  }
  if (action.purpose === 'cart') localStorage.setItem('cart', JSON.stringify(prods));
  const updatedSt = {
    [action.purpose] : prods
  }
  return updateObject( state, updatedSt );
};

/*const delProduct = (state, action) => {
  const prodIndex = state.products.indexOf(state.products.find(prod => prod.id===action.id));
  const cartIndex = action.id;
  const updatedProd = updateObject( state.products[prodIndex], 
    { ['count']: 0,
      ['cost']: 0,
      ['inCart']: false,
     })
  const cartProds = [];
  const prods = [];
  for ( let key in state.cart ) {
    if (key!==cartIndex) cartProds.push({...state.cart[key]});
  }
  for ( let key in state.products ) {
    key===prodIndex? 
      prods.push({...updatedProd })
      : prods.push({...state.products[key]});
  }
  localStorage.setItem('cart', JSON.stringify(cartProds));
  const updatedSt = {
    cart : cartProds,
    products: prods
  }
  return updateObject( state, updatedSt );
};*/

const purchaseProduct = ( state, action ) => {
    const index = state.products.indexOf(state.products.find(prod => prod.id===action.id));
    const updatedProd = updateObject( state.products[index], { ['count']: 1 })
    const fetchedProds = [];
    for ( let key in state.products ) {
      key==index ?
        fetchedProds.push({...updatedProd })
        : fetchedProds.push({...state.products[key]});
    }
    return updateObject( state, {products: fetchedProds} );
  };

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.INCREMENT: return incProduct( state, action );
        case actionTypes.DECREMENT: return decProduct(state, action);
        case actionTypes.FETCH_PRODUCTS_START: return fetchProductsStart( state, action );
        case actionTypes.FETCH_PRODUCTS_SUCCESS: return fetchProductsSuccess( state, action );
        case actionTypes.FETCH_PRODUCTS_FAIL: return fetchProductsFail( state, action );
        case actionTypes.PURCHASE_PRODUCT: return purchaseProduct( state, action );
        default: return state;
    }
};

export default reducer;
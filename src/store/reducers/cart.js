import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    cart: [],
    prodInfo: [],
    number: 1,
};

const addingStart = ( state, action ) => {
    return updateObject( state, { loading: false } );
};

const addingSuccess = ( state, action ) => {
    const newCartItem = updateObject( {}, {        
        id: action.id,
        prodId: action.prodId,
        count: action.count,
        price: action.price,
        cost: action.price*action.count,
        title: action.title
     } );
    return updateObject( state, {
        loading: false,
        cart: state.cart.concat( newCartItem )
    } );
};

const deleteCartProd = ( state, action ) => {
    const cartProds = [];
    for ( let key in state.cart ) {
      if (state.cart[key].id!==action.id) cartProds.push({...state.cart[key]});
    }
    return updateObject( state, { cart : cartProds } );
};

const addingFail = ( state, action ) => {
    return updateObject( state, { loading: false } );
};

const fetchCartProdInfoSuccess = ( state, action ) => {
    return updateObject( state, {
        cart: state.cart.concat( action.cartItem )
    } );
};

const fetchCartNumber = ( state, action ) => {
    return updateObject( state, { number: action.number } );
};

const fetchCartFail = ( state, action ) => {
    return updateObject( state, { loading: false } );
};

const fetchCartProdInfoFail = ( state, action ) => {
    return updateObject( state, { loading: false } );
};

const cartReducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.ADD_TO_CART_START: return addingStart( state, action );
        case actionTypes.ADD_TO_CART_SUCCESS: return addingSuccess( state, action )
        case actionTypes.ADD_TO_CART_FAIL: return addingFail( state, action );
        case actionTypes.FETCH_CART_NUMBER: return fetchCartNumber( state, action );
        case actionTypes.FETCH_CART_FAIL: return fetchCartFail( state, action );
        case actionTypes.FETCH_CART_PROD_INFO_SUCCESS: return fetchCartProdInfoSuccess( state, action );
        case actionTypes.FETCH_CART_PROD_INFO_FAIL: return fetchCartProdInfoFail( state, action );
        case actionTypes.CART_DELETE_PRODUCT: return deleteCartProd( state, action );
        default: return state;
    }

};

export default cartReducer;